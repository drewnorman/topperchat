<?php

include "../php/sqler.class.php";

session_start();

$sqler = new sqler();

$email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING);

$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);

$sqler->sendQuery("Select id from user where user.email='$email'");

// Duplicate email so refuse the registration
if ($sqler->getRow()) {
    echo 0;
}
else {
    if(!$stmt = $sqler->con->prepare("INSERT INTO user (email, password) VALUES (?,?)"))
    {
        echo "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error;
    }


    if(!$stmt->bind_param("ss", $email, $password))
    {
        echo "Bind fail (" . $stmt->errno . ") " . $stmt->error;
    }

    $password = $sqler->hashPass($password);

    if($stmt->execute())
    {
        echo 1; // Success
    }
    else
    {
        $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
        $stmt->close();
        echo $error;
    }
}
