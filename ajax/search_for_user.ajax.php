<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 11/21/2016
 * Time: 11:30 PM
 */

include "../php/sqler.class.php";

session_start();

$sqler = new sqler();

$keyword = filter_input(INPUT_POST, "keyword", FILTER_SANITIZE_STRING);
$currentUserEmail = $_SESSION["email_user"];

$sqler->sendQuery("Select email from user where user.email like '%$keyword%'");

// Assume no results
$results = [];
while ($row = $sqler->getRow()) {
    $email = $row['email'];
    $results[] = "<p onclick='addParticipant(this, \"$currentUserEmail\");' title='Click to Add User as Participant'>$email</p>";
}

// If no results
if (empty($results)) {
    // Echo 0 since there were no results
    echo 0;
}
else {
    // Echo the results (converting all of the array contents into a single string)
    echo implode("", $results);
}

