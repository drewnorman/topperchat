<?php

spl_autoload_register(function ($class) {
    include 'php/' . $class . '.class.php';
});

session_start();


$head = "<head>
            <link rel='stylesheet' type='text/css' href='css/main.css'>
            <script type='text/javascript' src='js/jquery-latest.js'></script>
            <script type='text/javascript' src='js/main.js'></script>
            <title>TopperChat</title>
        </head>";

if (isset($_SESSION['id_user'])) {
    $body = "<body>" . new topperchat_window() .
            "</body>";
}
else {
	$body = "<body>
	<img id='logo' src='img/topper_chat_logo.png'></br>

		<div class='authenticate_container'>
			<div id='login_container'>
                        <p>Login</p></br>
                        <label id='login_email_label'>Email:</label>
                        <input id='login_email' type='text'></br>
                        <label id='login_pass_label'>Password:</label>
                        <input id='login_password' type='password'></br>
                        <p id='invalid_login_message'></p>
                        <button id='login_button' type='button' onclick='login();'>Login</button>
                    	</div>
                    	<div id='register_container'>
                        <p>Register</p></br>
                        <label id='register_user_label'>Email:</label>
                        <input id='register_email' type='text'></br>
                        <label id='register_pass_label'>Password:</label>
                        <input id='register_password' type='password'></br>
                        <label id='confirm_label'>Confirm Password:</label>
                        <input id='confirm_password' type='password'></br>
                        <p id='invalid_registration_message'></p>
                        <button id='register_button' type='button' onclick='register();'>Register</button>
                    	</div>
                </div></body>";
}



echo $head . $body;
?>
